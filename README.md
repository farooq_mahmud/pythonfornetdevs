# Python for .NET Developers #

This repository contains the code sample I reviewed in my brown bag.

## How do I get set up? ##

### Prerequisites ###

* [Python 3.5.2](https://www.python.org/downloads/) - best to install Python to c:\python35 so it is easy to find
* Visual Studio 2015
* If not already installed - [Python tools for Visual Studio](https://www.visualstudio.com/en-us/features/python-vs.aspx) 

## Running the sample ##

* Clone this repo
* Open the SimpleApp solution
* Hit F5 to start the app in the debugger - alternatively you can type `python main.py http://demo0698183.mockable.io/music` from the command-line

## Problems, questions, or feedback? ##

Send me an [email](mailto:farooq.mahmud@slalom.com). 