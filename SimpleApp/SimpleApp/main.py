import sys
from app import App

def main(argv):
    if len(argv) != 1:
        printHelp()
        return
    
    response = input("Press a key to continue or 'q' to quit.")
    
    if response == "q":
        return

    url = argv[0]
    app = App(url)
    app.run()

    response = input("Done. Press a key to quit.")
    return

def printHelp():
    print("USAGE: main.py [url]")

if __name__ == "__main__":
    main(sys.argv[1:])
