﻿import requests
import json
from exceptions import DownloadException

class App():
    def __init__(self, url):
        self.__url = url

    def run(self):
        response = self.download()
        
        if response.status_code != 200:
            raise DownloadException("Download failed with status code {0}.".format(response.status_code))

        self.deserialize(response)

    def deserialize(self, response):
        
        try:
            json = response.json()
            playlists = json["playlists"]
            print("Downloaded {0} playlists.".format(len(playlists)))
            
            for playlist in playlists:
                print("{0} ({1} tracks)".format(playlist["name"], playlist["tracks"]["total"]))
        except Exception as e:
            print(e)
            raise

    def download(self):
        print("Downloading data from '{0}'...".format(self.__url))
        response = requests.get(self.__url)
        return response
